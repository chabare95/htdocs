var getLinks = function() {
    return [
        'http://bitbucket.com/',
        'http://www.drive.google.com/',
        'http://github.com',
        'http://www.animehaven.org/',
        'http://www.crunchyroll.com/home/queue',
        'http://myanimelist.net/animelist/chabare',
        'http://www.congstar.com/',
        'http://www.meine-ksk.de/',
        'http://www.paypal.de/',
        'http://www.theverge.com/',
        'http://www.theverge.com/',
        'http://www.wired.com/',
        'http://www.sight-board.de/tu-darmstadt/',
        'https://moodle.informatik.tu-darmstadt.de/',
        'https://www.tucan.tu-darmstadt.de/',
        'http://www.amazon.de/',
        'http://www.humblebundle.com/',
        'http://store.steampowered.com/',
        'http://www.4chan.org/b/',
        'http://www.reddit.com/',
        'https://web.whatsapp.com/',
        'http://www.netflix.com/',
        'http://www.twitch.tv/directory/following',
        'http://www.youtube.com/feed/subscriptions',
        'http://www.codeacademy.com',
        'http://www.forecast.io',
        'http://www.imgur.com'
    ];
}

var getLinkNames = function() {
    return [
        'BitBucket',
        'Drive',
        'Github',
        'Animehaven',
        'Crunchyroll',
        'MyAnimeList',
        'Congstar',
        'KSK',
        'PayPal',
        'ExtremeTech',
        'TheVerge',
        'Wired',
        'Campus-Navi',
        'Moodle',
        'TuCan',
        'Amazon',
        'HumbleBundle',
        'Steam',
        '4Chan',
        'Reddit',
        'WhatsApp',
        'Netflix',
        'Twitch',
        'YouTube',
        'CodeAcademy',
        'Forecast',
        'Imgur'
    ];
}

var getHeaders = function() {
    return [
        'Clouds',
        'Anime',
        'Money',
        'News',
        'University',
        'Shops',
        'Social',
        'Streaming',
        'Stuff'
    ];
}
